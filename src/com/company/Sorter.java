package com.company;

import model.Worker;

import java.util.Comparator;
import java.util.List;

public class Sorter implements Comparator<Worker> {
    @Override
    public int compare(Worker o1, Worker o2) {
//        int diffName = o1.getName().compareToIgnoreCase(o2.getName());
//        System.out.println(diffName);
//        return diffName;

//        return o1.getName().compareToIgnoreCase(o2.getName());

        Comparator<Worker> comparator = Comparator.comparing(Worker::getName);
        comparator.thenComparingDouble(Worker::getSalary);
        comparator.thenComparingDouble(Worker::getYearSalary);
        return comparator.compare(o1, o2);
    }

}
