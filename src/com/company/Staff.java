package com.company;

import model.Worker;

import java.util.Comparator;
import java.util.List;

public class Staff {
    public Staff() {
    }

    private List<Worker> workerList = Generator.generate();

    public void print() {
        for (Worker worker : workerList) {
            worker.print();
        }
    }

    public void sort() {
        Sorter sorter = new Sorter();
        workerList.sort(sorter);
    }
}
