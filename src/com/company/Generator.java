package com.company;

import model.*;

import java.util.ArrayList;
import java.util.List;

public final class Generator {
    private static void Generator() {
    }

    public static List<Worker> generate() {
        List<Worker> workerList = new ArrayList<>();
        workerList.add(new Stavka("Иванова Елена Львовна", "зам директора", 9500));
        workerList.add(new PerHour("Вакуленко Дмитрий Владимирович", "дизайнер", 50, 7));
        workerList.add(new Procent("Коренькова Анна Павловна", "менеджер по продажам", 51000, 65000, 7));
        workerList.add(new ProcentAndStavka("Коренькова Татьяна Сергеевна", "менеджер по продажам", 50000, 65000, 3, 1000));

        return workerList;
    }


}
