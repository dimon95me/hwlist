package model;

public class ProcentAndStavka extends Procent {
    private int stavka;

    public ProcentAndStavka(String name, String profession, int firstHalfOfYear, int secondHalfOfYear, int soldPercent, int stavka) {
        super(name, profession, firstHalfOfYear, secondHalfOfYear, soldPercent);
        this.stavka = stavka;
    }
}
