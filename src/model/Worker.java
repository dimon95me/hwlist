package model;


public class Worker {
    protected String name;
    protected String profession;
    protected float salary;

//    public Worker() {
//    }

    public Worker(String name, String profession, float salary) {
        this.name = name;
        this.profession = profession;
        this.salary = salary;
    }

    public float getSalary() {
        return salary;
    }

    public void print() {
        System.out.println("----------");
        System.out.println(name + " " + profession + " " + salary);
    }

    public String getName() {
        return name;
    }

    public float getYearSalary() {
        return salary * 12;
    }
}
