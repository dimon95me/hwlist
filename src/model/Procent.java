package model;

public class Procent extends Worker {
    private int firstHalfOfYear;
    private int secondHalfOfYear;
    private int soldPercent;

    public Procent(String name, String profession, int firstHalfOfYear, int secondHalfOfYear, int soldPercent) {
        super(name, profession, ((firstHalfOfYear+secondHalfOfYear)/12));
        this.firstHalfOfYear = firstHalfOfYear;
        this.secondHalfOfYear = secondHalfOfYear;
        this.soldPercent = soldPercent;
    }


}
